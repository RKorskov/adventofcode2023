package tld.localhost.AdventOfCode2023;

record D02GameOutcome(int red, int green, int blue) implements Comparable {
    @Override
    public int compareTo(Object o) { // {(x, y) such that x.compareTo(y) <= 0}
        if (o == null || o.getClass() != D02GameOutcome.class) throw new ClassCastException("uncomparable classes");
        D02GameOutcome ogo = (D02GameOutcome) o;
        if (ogo.blue() <= blue && ogo.green() <= green && ogo.red() <= red) return 1;
        // if(ogo.blue() == blue && ogo.green() == green && ogo.red() == red) return 0;
        return -1;
    }

    public int power() {
        return red * green * blue;
    }

    public String toString() {
        return String.format("(%d, %d, %d) ", red, green, blue);
    }

    public D02GameOutcome getMinSet(D02GameOutcome go) {
        return new D02GameOutcome(Math.max(red, go.red()), Math.max(green, go.green()), Math.max(blue, go.blue()));
    }
}
