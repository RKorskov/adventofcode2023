package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 8
 * Haunted Wasteland
 * TODO: 2 too long
 */
public class Day08 {
    // final static String FILE = "test08-1.txt";
    // final static String FILE = "test08-2.txt";
    // final static String FILE = "test08-3.txt";
    final static String FILE = "08.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day08.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        Map<String,D08NextHop> hops = parse(lines);
        System.out.println(part1(hops, "AAA", "ZZZ"));
        System.out.println(part2(hops)); // todo
    }

    /**
     * Starting at `AAA`, follow the left/right instructions.
     * How many steps are required to reach `ZZZ`?
     */
    private static int part1(final Map<String,D08NextHop> hops, final String srcNode, final String dstNode) {
        int stepsCount = 0;
        final byte[] hopLoop = hops.get("loop").base().getBytes();
        String curN = srcNode;
        for(int ptr = 0; !curN.equals(dstNode); ptr = ++ptr < hopLoop.length ? ptr : 0) {
            ++stepsCount;
            final D08NextHop nextHop = hops.get(curN);
            if(nextHop == null) {
                System.out.printf("nextHop == null: %s  %s  %d  %s\n", curN, hops.get(curN), ptr, nextHop);
                return -1;
            }
            byte c = hopLoop[ptr];
            curN = switch (c) {
                case 'L' -> nextHop.toLeft();
                case 'R' -> nextHop.toRight();
                default -> null;
            };
            if(curN == null) {
                System.out.printf("curN == null: %s  %d  %c  %s\n", hops.get("loop"), ptr, c, nextHop);
                return -2;
            }
        }
        return stepsCount;
    }

    static Map<String,D08NextHop> parse(final List<String> lines) {
        Map<String,D08NextHop> hops = new HashMap<>(lines.size());
        hops.put("loop", new D08NextHop(lines.get(0), null, null));
        for(String line: lines) {
            D08NextHop hop = D08NextHop.fromString(line);
            // System.out.println(hop);
            if(hop != null)
                hops.put(hop.base(), hop);
        }
        return hops;
    }

    /**
     * Simultaneously start on every node that ends with `A`.
     * How many steps does it take before you're only on nodes that end with `Z`?
     */
    private static long part2(final Map<String,D08NextHop> hops) { // todo: too long!
        String[] nodesA = hops.keySet().stream().filter(s -> s.charAt(2) == 'A').toArray(String[]::new);
        System.out.println(Arrays.toString(nodesA));
        long stepsCount = 0;
        final byte[] hopLoop = hops.get("loop").base().getBytes();
        // System.out.printf("ruleset: %s\n", hops.get("loop").base());
        for (int ptr = 0; Stream.of(nodesA).filter(s -> s.charAt(2) != 'Z').count() > 0; ptr = ++ptr < hopLoop.length ? ptr : 0) {
            // System.out.printf("%s  %d\n", Arrays.toString(nodesA), Stream.of(nodesA).filter(s -> s.charAt(2) != 'Z').count());
            ++stepsCount;
            final byte c = hopLoop[ptr];
            for(int i = 0; i < nodesA.length; ++i) {
                final D08NextHop nextHop = hops.get(nodesA[i]);
                if (nextHop == null) {
                    System.out.printf("nextHop == null: %s  %s  %d  %s\n", nodesA[i], hops.get(nodesA[i]), ptr, nextHop);
                    return -1;
                }
                nodesA[i] = switch (c) {
                    case 'L' -> nextHop.toLeft();
                    case 'R' -> nextHop.toRight();
                    default -> null;
                };
                if (nodesA[i] == null) {
                    System.out.printf("curN == null: %s  %d  %c  %s\n", hops.get("loop"), ptr, c, nextHop);
                    return -2;
                }
            }
            if(stepsCount % (1024*1024*96) == 0)
                System.out.println(Arrays.toString(nodesA));
        }
        return stepsCount;
    }
}
