package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 21
 * Step Counter
 * TODO: 1 2
 */
public class Day21 {
    final static String FILE = "test/21-1.txt";
    // final static String FILE = "21.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day21.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        String[] initStr = lines.get(0).split(",");
        System.out.println(part1(initStr)); // todo
        System.out.println(part2(initStr)); // todo
    }

    /**
     * Starting from the garden plot marked `S` on your map,
     * how many garden plots could the Elf reach in exactly 64 steps?
     */
    private static long part1(final String[] initStr) { // todo
        return -1;
    }

    /**
     */
    private static long part2(final String[] initStr) { // todo
        return -1;
    }
}
