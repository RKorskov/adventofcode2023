package tld.localhost.AdventOfCode2023;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

/*
 * AoC 2023
 * Day 4
 * Scratchcards
 * How many points are they worth in total?
 */
public class Day04 {
    // final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/test/resources/test04-1.txt";
    // final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/test/resources/test04-2.txt";
    final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/main/resources/04.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(new File(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        List<D04ScratchCard> cards = new ArrayList<>(lines.size());
        for(String cline: lines)
            cards.add(D04ScratchCard.fromString(cline));
        System.out.println(part1(cards));
        System.out.println(part2(cards));
    }

    /**
     * How many points are they worth in total?
     */
    private static long part1(final List<D04ScratchCard> cards) {
        long sum = 0;
        for(D04ScratchCard card: cards) {
            int cscore = 0;
            for (int i = 0; i < card.numWin.length; ++i) {
                final int curWiN = card.numWin[i];
                for (int j = 0; j < card.numYour.length; ++j)
                    if (curWiN == card.numYour[j])
                        cscore = cscore == 0 ? 1 : cscore * 2;
            }
            sum += cscore;
        }
        return sum;
    }

    private static int part2(final List<D04ScratchCard> cards) {
        int numCards = 0;
        Deque<D04ScratchCard> gifts = new LinkedList<>(cards);
        int[] remnants = new int[cards.size()+1];
        for(;!gifts.isEmpty();) {
            final D04ScratchCard card = gifts.pop();
            if(card == null) continue;
            ++remnants[card.id];
            // System.out.println(card);
            int ccount = card.id;
            for (int i = 0; i < card.numWin.length; ++i) {
                final int curWiN = card.numWin[i];
                for (int j = 0; j < card.numYour.length; ++j)
                    if (curWiN == card.numYour[j]) {
                        ++ccount;
                        gifts.addLast(getCard(cards, ccount));
                        // System.out.printf("add: %d\n", ccount);
                    }
            }
        }
        return IntStream.of(remnants).sum();
    }

    /**
     * get card with given ID from list
     */
    private static D04ScratchCard getCard(List<D04ScratchCard> cards, int curWiN) {
        for(D04ScratchCard card: cards)
            if(card.id == curWiN)
                return card;
        return null;
    }
}
