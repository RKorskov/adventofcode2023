package tld.localhost.AdventOfCode2023;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AAA = (BBB, CCC)
 * base = (toLeft, toRight)
 */
record D08NextHop(String base, String toLeft, String toRight) {
    private final static Pattern pat = Pattern.compile("^ *([0-9A-Z]+) *= *[(]([0-9A-Z]+), *([0-9A-Z]+)[)] *$");

    public String toString() {
        return String.format("%s = (%s, %s)", base, toLeft, toRight);
    }

    public static D08NextHop fromString(final String src) {
        final Matcher m = pat.matcher(src);
        if(!m.find()) return null;
        return new D08NextHop(m.group(1), m.group(2), m.group(3));
    }
}
