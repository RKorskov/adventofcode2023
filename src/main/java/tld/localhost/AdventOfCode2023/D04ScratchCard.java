package tld.localhost.AdventOfCode2023;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class D04ScratchCard {
    final int id;
    final int[] numWin, numYour;

    private D04ScratchCard(final int cid, final int[] nwin, final int[] nyour) {
        this.id = cid;
        this.numWin = nwin;
        this.numYour = nyour;
    }

    public String toString() {
        return String.format("%d: %s | %s", id, Arrays.toString(numWin), Arrays.toString(numYour));
    }

    // Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
    public static D04ScratchCard fromString(final String cardLine) {
        String[] shards = cardLine.split(":? +");
        List<Integer> nwin = null, nyour = null;
        int cid = Integer.parseInt(shards[1]);
        List<Integer> car = new ArrayList<>();
        for (int i = 2; i < shards.length; ++i) {
            if (shards[i].equals("|")) {
                nwin = car;
                car = new ArrayList<>();
            } else
                car.add(Integer.parseInt(shards[i]));
        }
        nyour = car;
        return new D04ScratchCard(cid, nwin.stream().mapToInt(n -> n).toArray(), nyour.stream().mapToInt(n -> n).toArray());
    }
}
