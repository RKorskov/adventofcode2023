package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 25
 * Snowverload
 * TODO: 1
 */
public class Day25 {
    final static String FILE = "test/25-1.txt";
    // final static String FILE = "25.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day25.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Find the three wires you need to disconnect in order to divide the components into two separate groups.
     * What do you get if you multiply the sizes of these two groups together?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
