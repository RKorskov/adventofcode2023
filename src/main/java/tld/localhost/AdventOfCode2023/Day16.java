package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 16
 * The Floor Will Be Lava
 * TODO: 1 2
 */
public class Day16 {
    final static String FILE = "test/16-1.txt";
    // final static String FILE = "16.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day16.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * The light isn't energizing enough tiles to produce lava;
     * to debug the contraption, you need to start by analyzing the current situation.
     * With the beam starting in the top-left heading right, how many tiles end up being energized?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
