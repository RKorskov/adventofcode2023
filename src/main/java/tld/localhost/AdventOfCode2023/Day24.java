package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 24
 * Never Tell Me The Odds
 * TODO: 1 2
 */
public class Day24 {
    final static String FILE = "test/24-1.txt";
    // final static String FILE = "24.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day24.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Considering only the X and Y axes, check all pairs of hailstones' future paths for intersections.
     * How many of these intersections occur within the test area?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
