package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 13
 * Point of Incidence
 * TODO: 1 2
 */
public class Day13 {
    // final static String FILE = "test/13-1.txt";
    final static String FILE = "13.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day13.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Find the line of reflection in each of the patterns in your notes.
     * What number do you get after summarizing all of your notes?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
