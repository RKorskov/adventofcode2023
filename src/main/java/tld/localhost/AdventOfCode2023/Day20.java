package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 20
 * Pulse Propagation
 * TODO: 1 2
 */
public class Day20 {
    final static String FILE = "test/20-1.txt";
    // final static String FILE = "20.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day20.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Consult your module configuration; determine the number of low pulses and high pulses that would be sent
     * after pushing the button 1000 times, waiting for all pulses to be fully handled after each push of the button.
     * What do you get if you multiply the total number of low pulses sent by the total number of high pulses sent?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
