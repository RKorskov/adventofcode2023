package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 15
 * Lens Library
 * TODO: 2
 */
public class Day15 {
    final static String FILE = "test/15-1.txt";
    // final static String FILE = "15.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day15.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        String[] initStr = lines.get(0).split(",");
        System.out.println(part1(initStr));
        System.out.println(part2(initStr)); // todo
    }

    /**
     * Run the HASH algorithm on each step in the initialization sequence.
     * What is the sum of the results?
     */
    private static long part1(final String[] initStr) { // todo
        long sum = 0;
        for(int i = 0; i < initStr.length; ++i)
            sum += hashFunc(initStr[i]);
        return sum;
    }

    /**
     * - Determine the ASCII code for the current character of the string.
     * - Increase the current value by the ASCII code you just determined.
     * - Set the current value to itself multiplied by 17.
     * - Set the current value to the remainder of dividing itself by 256.
     * @param src
     * @return hash
     */
    private static int hashFunc(final String src) {
        int hash = 0;
        for(int i = 0; i < src.length(); ++i)
            hash = (hash + src.charAt(i) * 17) & 0xFF;
        return hash;
    }

    /**
     * same as hashFunc, but only on 1st 2 chars
     */
    private static int hashFunc2(final String src) {
        return ((src.charAt(0) + src.charAt(1)) * 17) & 0xFF;
    }

    /**
     * ... follow the initialization sequence.
     * What is the focusing power of the resulting lens configuration?
     */
    private static long part2(final String[] initStr) { // todo
        HashMap<Integer, Deque<D15Lens>> boxes = new HashMap<>(256);
        for(int i = 0; i < initStr.length; ++i) {
            final D15Lens lens = new D15Lens(initStr[i]);
            final int boxIdx = hashFunc2(initStr[i]);
            if(!boxes.containsKey(boxIdx)) boxes.put(boxIdx, new LinkedList<>());
            // final Deque<D15Lens> lenses = boxes.get(boxIdx);
            final LinkedList<D15Lens> lenses = (LinkedList<D15Lens>)boxes.get(boxIdx);
            if(lens.focalLength < 1) { // remove this lens from box with boxIdx
                D15Lens rcl = null;
                for(D15Lens cl : lenses)
                    if(cl.label.equals(lens.label)) {
                        rcl = cl;
                        break;
                    }
                if(rcl != null)
                    lenses.remove(rcl);
                continue;
            }
            // if(lens.focalLength > 0) // add/replace lens
            for(int j = 0; j < lenses.size(); ++j) {
                D15Lens cl = lenses.get(j);
                if(cl.label.equals(lens.label)) {
                    lenses.set(j, lens);
                    break;
                }
            }
        }
        long power = 0;
        for(int idx = 0; idx < 256; ++idx) {
            if(!boxes.containsKey(idx)) continue;
            power += evalPower(boxes.get(idx)) * (idx + 1);
        }
        return power;
    }

    /**
     * The focusing power of a single lens is the result of multiplying together:
     * - One plus the box number of the lens in question.
     * - The slot number of the lens within the box: 1 for the first lens, 2 for the second lens, and so on.
     * - The focal length of the lens.
     */
    private static long evalPower(final Deque<D15Lens> lenses) {
        int pwr = 0;
        for(int mul = 1; lenses.size() > 0; ++mul) {
            pwr += mul * lenses.removeFirst().focalLength;
        }
        return pwr;
    }
}
