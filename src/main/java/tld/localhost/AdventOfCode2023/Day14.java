package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 14
 * Parabolic Reflector Dish
 * TODO: 2 too long
 */
public class Day14 {
    // final static String FILE = "test/14-1.txt";
    final static String FILE = "14.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day14.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines, 1_000_000_000)); // todo
    }

    /**
     * Tilt the platform so that the rounded rocks all roll north.
     * Afterward, what is the total load on the north support beams?
     */
    private static long part1(final List<String> lines) { // todo
        final D14Cell[][] map = readMap(lines);
        tiltPlatformNorth(map);
        return getLoad(map);
    }

    private static long getLoad(final D14Cell[][] map) {
        long sum = 0;
        for(int j = 0; j < map.length; ++j)
            for(int i = 0; i < map[j].length; ++i) {
                if(map[j][i] != D14Cell.ROCK) continue;
                final int rockLoad = map.length - j;
                sum += rockLoad;
            }
        return sum;
    }

    private static D14Cell[][] readMap(final List<String> lines) {
        final D14Cell[][] map = new D14Cell[lines.size()][];
        for(int j = 0; j < lines.size(); ++j) {
            final byte[] row = lines.get(j).getBytes();
            map[j] = new D14Cell[row.length];
            for(int i = 0; i < row.length; ++i) {
                map[j][i] = switch(row[i]) {
                    case 'O' -> D14Cell.ROCK;
                    case '#' -> D14Cell.IMMOVABLE;
                    default -> D14Cell.EMPTY;
                };
            }
        }
        return map;
    }

    /**
     * Start by tilting the lever so all of the rocks will slide north as far as they will go
     */
    private static void tiltPlatformNorth(final D14Cell[][] map) {
        for(int i = 0; i < map[0].length; ++i) {
            for(int j = 1; j < map.length; ++j) {
                for(int y = j; y > 0 && map[y][i] == D14Cell.ROCK; --y)
                    if(map[y-1][i] == D14Cell.EMPTY) {
                        map[y-1][i] = D14Cell.ROCK;
                        map[y][i] = D14Cell.EMPTY;
                    }
            }
        }
    }

    private static void printMap(final D14Cell[][] map) {
        for(int j = 0; j < map.length; ++j) {
            StringBuilder sb = new StringBuilder(map[j].length + 4);
            for (int i = 0; i < map[j].length; ++i) {
                sb.append(switch(map[j][i]) {
                    case ROCK -> '0';
                    case IMMOVABLE -> '#';
                    default -> '.';
                });
            }
            System.out.println(sb);
        }
    }

    /**
     * Each cycle tilts the platform four times so that the rounded rocks roll north, then west, then south, then east.
     * After each tilt, the rounded rocks roll as far as they can before the platform tilts in the next direction.
     * Run the spin cycle for 1_000_000_000 cycles.
     * Afterward, what is the total load on the north support beams?
     */
    private static long part2(final List<String> lines, final long cycles) { // todo: too long
        final D14Cell[][] map = readMap(lines);
        final long[] bitmap0 = getBitmap(map);
        Map<Long, long[]> bitmaps = new HashMap<>(256);
        Map<Long, D14Cell[][]> mapCase = new HashMap<>(256);
        bitmaps.put(0L, bitmap0);
        mapCase.put(0L, copyMap(map));
        mainLoop2: for(long i = 1; i <= cycles; ++i) {
            tiltPlatformNorth(map);
            tiltPlatformWest(map);
            tiltPlatformSouth(map);
            tiltPlatformEast(map);
            final long[] bitmap1 = getBitmap(map);
            for(Long k: bitmaps.keySet()) {
                if(cmpBitmap(bitmap1, bitmaps.get(k))) {
                    System.out.printf("--=>>> Loop detected! Cycles from %d to %d\n", k, i);
                    final long pOff = k;
                    final long pDiv = i - pOff;
                    final long n1G = (cycles - pOff) % pDiv + pOff;
                    return getLoad(mapCase.get(n1G));
                }
            }
            bitmaps.put(i, bitmap1);
            mapCase.put(i, copyMap(map));
        }
        return getLoad(map);
    }

    private static D14Cell[][] copyMap(final D14Cell[][] map) {
        D14Cell[][] cmap = new D14Cell[map.length][];
        for(int i = 0; i < map.length; ++i)
            cmap[i] = Arrays.copyOf(map[i], map[i].length);
        return cmap;
    }

    final static int FRAG2LONG = 63; // to stay in unsigned zone
    final static int FRAG_LENGTH = FRAG2LONG;
    /**
     * packing map into bit string
     * @param map
     * @return bit-string packed map
     */
    private static long[] getBitmap(final D14Cell[][] map) {
        final int maxlen = map.length * map[0].length / FRAG_LENGTH + 1; // length of long
        long[] bits = new long[maxlen];
        int ptr = 0;
        for(int j = 0; j < map.length; ++j)
            for(int i = 0; i < map[j].length; ++i) {
                if (map[j][i] == D14Cell.ROCK)
                    setBit(bits, ptr);
                ++ptr;
            }
        return bits;
    }

    private static void setBit(final long[] bits, final int ptr) {
        final int p = ptr / FRAG_LENGTH, q = ptr % FRAG_LENGTH;
        bits[p] |= 1L << q;
    }

    private static boolean cmpBitmap(final long[] bits0, final long[] bits1) {
        if(bits0 == null || bits1 == null || bits0.length != bits1.length) return false;
        for(int i = 0; i < bits0.length; ++i)
            if(bits0[i] != bits1[i]) return false;
        return true;
    }

    private static void printBitmap(final long[] bits) {
        StringBuilder sb = new StringBuilder(bits.length * 10);
        final HexFormat hex = HexFormat.of().withUpperCase();
        for(int i = 0; i < bits.length; ++i)
            sb.append(hex.toHexDigits(bits[i]));
        System.out.println(sb);
    }

    private static void tiltPlatformWest(final D14Cell[][] map) {
        for(int j = 0; j < map.length ; ++j) {
            for(int i = 1; i < map[j].length; ++i) {
                for(int x = i; x > 0 && map[j][x] == D14Cell.ROCK; --x)
                    if(map[j][x - 1] == D14Cell.EMPTY) {
                        map[j][x - 1] = D14Cell.ROCK;
                        map[j][x] = D14Cell.EMPTY;
                    }
            }
        }
    }

    private static void tiltPlatformSouth(final D14Cell[][] map) {
        for(int i = 0; i < map[0].length; ++i) {
            for(int j = map.length - 2; j >= 0 ; --j) {
                for(int y = j; y < (map.length - 1) && map[y][i] == D14Cell.ROCK; ++y)
                    if(map[y+1][i] == D14Cell.EMPTY) {
                        map[y+1][i] = D14Cell.ROCK;
                        map[y][i] = D14Cell.EMPTY;
                    }
            }
        }
    }

    private static void tiltPlatformEast(final D14Cell[][] map) {
        for(int j = 0; j < map.length ; ++j) {
            for(int i = map[j].length - 2; i >= 0; --i) {
                for(int x = i; x < (map[j].length - 1) && map[j][x] == D14Cell.ROCK; ++x)
                    if(map[j][x + 1] == D14Cell.EMPTY) {
                        map[j][x + 1] = D14Cell.ROCK;
                        map[j][x] = D14Cell.EMPTY;
                    }
            }
        }
    }
}
