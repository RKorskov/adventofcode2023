package tld.localhost.AdventOfCode2023;

public class D07CCHand implements Comparable {
    final String hand;
    final int bid;
    private D07Kind kind;
    private int rank;
    D07CCHand(final String h, final int b) {
        hand = h;
        bid = b;
        kind = null;
        rank = 0;
    }

    public String toString() {
        return String.format("%s : %d : %s : %d", hand, bid, kind(), rank);
    }

    public D07Kind kind() {
        if(kind == null) {
            final char[] cs = new char[hand.length()];
            final int[] cnt = new int[cs.length];
            inspectCards(cs, cnt);
            if(rule1(cs, cnt))
                kind = D07Kind.FIVE_OF_A_KIND;
            else if(rule2(cs, cnt))
                kind = D07Kind.FOUR_OF_A_KIND;
            else if(rule3(cs, cnt))
                kind = D07Kind.FULL_HOUSE;
            else if(rule4(cs, cnt))
                kind = D07Kind.THREE_OF_A_KIND;
            else if(rule5(cs, cnt))
                kind = D07Kind.TWO_PAIR;
            else if(rule6(cs, cnt))
                kind = D07Kind.ONE_PAIR;
            else if(rule7(cs, cnt))
                kind = D07Kind.HIGH_CARD;
        }
        return kind;
    }
    public int bid() {return bid;}

    public int rank() {return rank;}
    public void setRank(final int r) {rank = r;}

    public void inspectCards(final char[] cs, final int[] cnt) {
        for(int i = 0, cp =  0; i < cs.length; ++i) {
            final char c = hand.charAt(i);
            boolean incCpF = true;
            for(int j = 0; j < cp; ++j)
                if(cs[j] == c) {
                    ++cnt[j];
                    incCpF = false;
                    break;
                }
            if(incCpF) {
                cs[cp] = c;
                cnt[cp] = 1;
                ++cp;
            }
        }
    }

    // Five of a kind, where all five cards have the same label: `AAAAA`
    private boolean rule1(final char[] cs, final int[] cnt) {
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 5)
                return true;
        return false;
    }

    // Four of a kind, where four cards have the same label and one card has a different label: `AA8AA`
    private boolean rule2(final char[] cs, final int[] cnt) {
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 4)
                return true;
        return false;
    }

    // Full house, where three cards have the same label, and the remaining two cards share a different label: `23332`
    private boolean rule3(final char[] cs, final int[] cnt) {
        boolean f2 = false, f3 = false;
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 2)
                f2 = true;
            else
                if(cnt[i] == 3)
                    f3 = true;
        return f2 & f3;
    }

    // Three of a kind, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: `TTT98`
    private boolean rule4(final char[] cs, final int[] cnt) {
        boolean f3 = false, f2 = false;
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 3)
                f3 = true;
            else if(cnt[i] == 2)
                f2 = true;
        return f3 & !f2;
    }

    // Two pair, where two cards share one label, two other cards share a second label, and the remaining card has a third label: `23432`
    private boolean rule5(final char[] cs, final int[] cnt) {
        int c2 = 0;
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 2)
                ++c2;
        return c2 == 2;
    }

    // One pair, where two cards share one label, and the other three cards have a different label from the pair and each other: `A23A4`
    private boolean rule6(final char[] cs, final int[] cnt) {
        boolean f2 = false;
        int c1 = 0;
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] == 2)
                f2 = true;
            else if(cnt[i] == 1)
                ++c1;
        return f2 && c1 == 3;
    }

    // High card, where all cards' labels are distinct: `23456`
    private boolean rule7(final char[] cs, final int[] cnt) { // max
        for(int i = 0; i < cnt.length; ++i)
            if(cnt[i] > 1)
                return false;
        return true;
    }

    // 32T3K 765
    public static D07CCHand fromString(final String src) {
        final String[] hb = src.split(" +");
        final String hand = hb[0];
        final int bid = Integer.parseUnsignedInt(hb[1]);
        return new D07CCHand(hand, bid);
    }

    @Override
    public int compareTo(Object o) {
        if(o.getClass() != D07CCHand.class) return -2;
        D07CCHand co = (D07CCHand) o;
        if(co.kind() == kind()) {
            for(int i = 0; i < hand.length(); ++i) {
                final char coi = co.hand.charAt(i);
                final char ci = hand.charAt(0);
                if (coi > ci)
                    return 1;
                if (coi < ci)
                    return -1;
            }
            return 0;
        }
        if(kind().ordinal() < co.kind().ordinal()) return 1;
        return -1;
    }
}
