package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 11
 * Cosmic Expansion
 * TODO: 1, 2
 */
public class Day11 {
    final static String FILE = "test11-1.txt";
    // final static String FILE = "11.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day11.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines));
    }

    /**
     * Expand the universe, then find the length of the shortest path between every pair of galaxies.
     * What is the sum of these lengths?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
