package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 12
 * Hot Springs
 * TODO: 1 2
 */
public class Day12 {
    // final static String FILE = "test/12-1.txt";
    final static String FILE = "12.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day12.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * For each row, count all of the different arrangements of operational and broken springs that meet the given criteria.
     * What is the sum of those counts?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
