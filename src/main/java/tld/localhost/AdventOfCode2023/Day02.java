package tld.localhost.AdventOfCode2023;

import java.io.File;
import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 2
 * Cube Conundrum
 * Determine which games would have been possible if the bag had been loaded with only 12 red cubes, 13 green cubes, and 14 blue cubes.
 * What is the sum of the IDs of those games?
 */
public class Day02 {
    // final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/test/resources/test01-1.txt";
    // final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/test/resources/test2.txt";
    final static String FILE = "/var/home/korskov/toys/AdventOfCode2023/src/main/resources/02.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(new File(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines, new D02GameOutcome(12, 13, 14))); // ok
        System.out.println(part2(lines));
    }

    private static int part1(final List<String> lines, final D02GameOutcome amountsLimitsLower) {
        // System.out.printf("Limits: %s\n", amountsLimitsLower);
        Map<Integer, List<D02GameOutcome>> games = parseData(lines);
        int sum = 0;
        for(Integer gid: games.keySet()) {
            List<D02GameOutcome> outs = games.get(gid);
            boolean gameInRange = true;
            for(D02GameOutcome outcome: outs) {
                // System.out.printf("%d: %s\n", gid, outcome);
                if(amountsLimitsLower.compareTo(outcome) <= 0) {
                    gameInRange = false;
                    break;
                }
            }
            if(gameInRange)
                sum += gid;
        }
        return sum;
    }

    private static Map<Integer, List<D02GameOutcome>> parseData(final List<String> lines) {
        Map<Integer, List<D02GameOutcome>> games = new HashMap<>();
        for(String sline: lines) {
            final List<D02GameOutcome> gouts = new ArrayList<>();
            final String[] gidOuts = sline.split(": *");
            final String[] outs = gidOuts[1].split("; *");
            final Integer gid = Integer.parseInt(gidOuts[0].split(" +")[1]);
            for(String outcome : outs) {
                int r = 0, g = 0, b = 0;
                final String[] os = outcome.split(",? +");
                for(int i = 0; i < os.length; i += 2) {
                    final int v = Integer.parseInt(os[i]);
                    switch (os[i + 1]) {
                        case "red" -> r = v;
                        case "blue" -> b = v;
                        case "green" -> g = v;
                    }
                }
                gouts.add(new D02GameOutcome(r, g, b));
            }
            games.put(gid, gouts);
            // System.out.printf("%d: ", gid); gouts.stream().forEach(System.out::print); System.out.println();
        }
        return games;
    }

    /**
     * The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together.
     * For each game, find the minimum set of cubes that must have been present. What is the sum of the power of these sets?
     */
    private static int part2(final List<String> lines) {
        Map<Integer, List<D02GameOutcome>> games = parseData(lines);
        int sum = 0;
        for(Integer gid: games.keySet()) {
            D02GameOutcome minSet = new D02GameOutcome(0, 0, 0);
            List<D02GameOutcome> outs = games.get(gid);
            for(D02GameOutcome outcome: outs) {
                minSet = minSet.getMinSet(outcome);
            }
            // System.out.printf("%d: %s\n", gid, minSet);
            sum += minSet.power();
        }
        return sum;
    }
}

