package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 9
 * Mirage Maintenance
 * TODO: 1, 2
 */
public class Day09 {
    // final static String FILE = "test09-1.txt";
    final static String FILE = "09.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day09.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        long[][] oasisData = new long[lines.size()][];
        for(int j = 0; j < lines.size(); ++j)
            oasisData[j] = Stream.of(lines.get(j).split(" +")).mapToLong(Long::parseLong).toArray();
        System.out.println(part1(oasisData));
        System.out.println(part2(lines));
    }

    /**
     * Analyze your OASIS report and extrapolate the next value for each history.
     * What is the sum of these extrapolated values?
     */
    private static long part1(final long[][] oasisData) { // todo
        long sum = 0;
        for(int dataRowIdx = 0; dataRowIdx < oasisData.length; ++dataRowIdx) {
            List<long[]> extData = new ArrayList<>();
            extData.add(oasisData[dataRowIdx]);
            for(int newDataRowIdx = 0; LongStream.of(extData.get(newDataRowIdx)).sum() != 0; ++newDataRowIdx) {
                final long[] prevRow = extData.get(newDataRowIdx);
                long[] nextRow = new long[extData.get(newDataRowIdx).length - 1];
                for(int i = 0; i < nextRow.length; ++i)
                    nextRow[i] = prevRow[i + 1] - prevRow[i];
                extData.add(nextRow);
            }
            // extData.stream().forEach(r -> System.out.println(Arrays.toString(r)));
            int val = 0;
            for(int i = extData.size() - 2; i >= 0; --i) {
                final long[] row = extData.get(i);
                final long n = row[row.length - 1];
                val += n;
            }
            // System.out.println(val);
            sum += val;
        }
        return sum;
    }

    /**
     */
    private static long part2(final List<String> lines) {
        return -1;
    }
}
