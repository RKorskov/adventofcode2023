package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 23
 * A Long Walk
 * TODO: 1 2
 */
public class Day23 {
    final static String FILE = "test/23-1.txt";
    // final static String FILE = "23.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day23.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Find the longest hike you can take through the hiking trails listed on your map.
     * How many steps long is the longest hike?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
