package tld.localhost.AdventOfCode2023;

public enum D14Cell {
    EMPTY, ROCK, IMMOVABLE;

    @Override
    public String toString() {
        return switch (this.name()) {
            case "EMPTY" -> ".";
            case "ROCK" -> "0";
            case "IMMOVABLE" -> "#";
            default -> "?";
        };
    }
}
