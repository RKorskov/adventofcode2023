package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 10
 * Pipe Maze
 * TODO: 1, 2
 */
public class Day10 {
    final static String FILE = "test10-1.txt";
    // final static String FILE = "10.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day10.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines));
    }

    /**
     * Find the single giant loop starting at S.
     * How many steps along the loop does it take to get from the starting position to the point farthest from the starting position?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
