package tld.localhost.AdventOfCode2023;

public enum D16MapObjects {
    EMPTY, MIRROR_LEFT, MIRROR_RIGHT, SPLITTER_Y, SPLITTER_X;

    public static D16MapObjects fromChar(final char c) throws IllegalArgumentException {
        switch(c) {
            case '.': return EMPTY;
            case '/': return MIRROR_LEFT;
            case '\\': return MIRROR_RIGHT;
            case '|': return SPLITTER_Y;
            case '-': return SPLITTER_X;
        };
        return EMPTY;
    }
}
