package tld.localhost.AdventOfCode2023;

/*
 * http://wiki.c2.com/?FizzBuzzTest=
 * Write a program that prints the numbers from 1 to 100.
 * But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
 * For numbers which are multiples of both three and five print “FizzBuzz”.
 *
 * ''It would be more interesting if the numbers were -50 to +50...''
 */
public class FizzBuzzTest {
    public static void main(final String[] args) {
        fizz(1, 100);
        fizz(-50, 50);
    }

    private static void fizz(final int rangeStart, final int rangeEnd) {
        System.out.printf("Range: %d to %d\n", rangeStart, rangeEnd);
        for(int i = rangeStart; i <= rangeEnd; ++i) {
            final boolean f3 = i % 3 == 0;
            final boolean f5 = i % 5 == 0;
            String msg = "";
            if(f3 || f5) {
                if(f3)
                    msg = "Fizz";
                if(f5)
                    msg += "Buzz";
            }
            else
                msg = String.valueOf(i);
            System.out.println(msg);
        }
    }
}
