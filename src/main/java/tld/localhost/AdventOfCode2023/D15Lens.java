package tld.localhost.AdventOfCode2023;

class D15Lens {
    final String label;
    //final int label;
    final int focalLength;

    D15Lens(final String src) {
        // this.label = (src.charAt(0) << 8) + src.charAt(1);
        this.label = src.substring(0,2);
        if(src.length() >= 4)
            this.focalLength = src.charAt(3) - '0';
        else
            this.focalLength = -1;
    }
}
