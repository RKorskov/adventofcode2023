package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 17
 * Clumsy Crucible
 * TODO: 1 2
 */
public class Day17 {
    final static String FILE = "test/17-1.txt";
    // final static String FILE = "17.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day17.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Directing the crucible from the lava pool to the machine parts factory,
     * but not moving more than three consecutive blocks in the same direction,
     * what is the least heat loss it can incur?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
