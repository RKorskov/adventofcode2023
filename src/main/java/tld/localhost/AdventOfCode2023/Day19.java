package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 19
 * Aplenty
 * TODO: 1 2
 */
public class Day19 {
    final static String FILE = "test/19-1.txt";
    // final static String FILE = "19.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day19.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Sort through all of the parts you've been given;
     * what do you get if you add together all of the rating numbers for all of the parts that ultimately get accepted?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
