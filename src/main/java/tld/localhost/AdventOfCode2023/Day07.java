package tld.localhost.AdventOfCode2023;

import java.io.File;
import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 7
 * Camel Cards
 * TODO: 1, 2
 */
public class Day07 {
    // final static String FILE = "test/07-1.txt";
    final static String FILE = "07.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day07.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines));
    }

    /**
     * Find the rank of every hand in your set.
     * What are the total winnings?
     */
    private static long part1(final List<String> lines) {
        // Map<String,Integer> cards = new HashMap<>(lines.size());
        List<D07CCHand> cards = new ArrayList<>();
        for(String line: lines) {
            // String[] kv = line.split(" +");
            // cards.put(kv[0], Integer.parseInt(kv[1]));
            cards.add(D07CCHand.fromString(line));
        }
        cards.sort((c0,c1) -> c0.compareTo(c1));
        for(int i = 0; i < cards.size(); ++i) {
            final D07CCHand h = cards.get(i);
            h.setRank(i + 1);
            System.out.println(h);
        }
        long sum = 0;
        for(int i = 0; i < cards.size(); ++i) {
            final D07CCHand h = cards.get(i);
            sum += h.bid() * h.rank();
        }
        return sum;
    }

    private static long part2(final List<String> lines) {
        return -1;
    }
}
