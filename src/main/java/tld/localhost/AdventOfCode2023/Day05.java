package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 5
 * If You Give A Seed A Fertilizer
 * What is the lowest location number that corresponds to any of the initial seed numbers?
 * TODO: 2 too long
 */
public class Day05 {
    // final static String FILE = "test05-1.txt";
    final static String FILE = "05.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day05.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        Map<String, List<D05Range>> map = parse(lines);
        System.out.println(part1(map));
        System.out.println(part2(map)); // todo
    }

    private static long part1(final Map<String, List<D05Range>> map) {
        List<D05Range> seeds = map.get("seeds");
        long loc = Long.MAX_VALUE;
        for(D05Range seed: seeds) {
            final long sid = seed.src();
            long dst = translate(map, "soil", sid);
            dst = translate(map, "fertilizer", dst);
            dst = translate(map, "water", dst);
            dst = translate(map, "light", dst);
            dst = translate(map, "temperature", dst);
            dst = translate(map, "humidity", dst);
            dst = translate(map, "location", dst);
            loc = Math.min(loc, dst);
            // System.out.printf("%d -> %d\n", sid, loc);
        }
        return loc;
    }

    private static long translate(final Map<String, List<D05Range>> map, final String key, final long val) {
        final List<D05Range> rngs = map.get(key);
        for(D05Range rng: rngs) {
            final OptionalLong dst = rng.apply(val);
            if(dst.isPresent()) {
                // System.out.printf("%s %s.apply(%d) -> %d\n", key, rng, val, dst.getAsLong());
                return dst.getAsLong();
            }
        }
        return val;
    }

    static Map<String,List<D05Range>> parse(final List<String> lines) {
        final Map<String, List<D05Range>> map = new HashMap<>();
        String keyName = null;
        for(String line: lines) {
            if(line.isEmpty()) continue;
            final String[] line1st = line.split(": *");
            switch (line1st[0]) {
                case "seeds": {
                    map.put("seeds",
                            Stream.of(line1st[1]
                                    .split(" +"))
                                    .map(s -> {
                                        long t = Long.parseLong(s);
                                        return new D05Range(t, t, 1);
                                    })
                                    .collect(Collectors.toList()));
                    break;
                }
                case "seed-to-soil map": {
                    keyName = "soil";
                    break;
                }
                case "soil-to-fertilizer map": {
                    keyName = "fertilizer";
                    break;
                }
                case "fertilizer-to-water map": {
                    keyName = "water";
                    break;
                }
                case "water-to-light map": {
                    keyName = "light";
                    break;
                }
                case "light-to-temperature map": {
                    keyName = "temperature";
                    break;
                }
                case "temperature-to-humidity map": {
                    keyName = "humidity";
                    break;
                }
                case "humidity-to-location map": {
                    keyName = "location";
                    break;
                }
                default: {
                    if(keyName != null) {
                        if (!map.containsKey(keyName))
                            map.put(keyName, new ArrayList<>());
                        List<D05Range> rngs = map.get(keyName);
                        final D05Range rng = D05Range.fromString(line);
                        rngs.add(rng);
                        // map.put(keyName, rngs);
                        // System.out.printf("%s : +%s\n", keyName, rng);
                    }
                }
            }
        }
        return map;
    }

    /**
     * Re-reading the almanac, it looks like the seeds: line actually describes ranges of seed numbers.
     * The values on the initial seeds: line come in pairs.
     * Within each pair, the first value is the start of the range and the second value is the length of the range.
     */
    private static long part2(final Map<String, List<D05Range>> map) { // todo: too long
        System.out.println("fixme! TOO long!");
        List<D05Range> seeds = map.get("seeds");
        long loc = Long.MAX_VALUE;
        for(int i = 0; i < seeds.size(); i += 2) {
            final long src = seeds.get(i).src();
            final long len = seeds.get(i+1).src() + src;
            for(long sid = src; sid < len; ++sid) {
                long dst = translate(map, "soil", sid);
                dst = translate(map, "fertilizer", dst);
                dst = translate(map, "water", dst);
                dst = translate(map, "light", dst);
                dst = translate(map, "temperature", dst);
                dst = translate(map, "humidity", dst);
                dst = translate(map, "location", dst);
                loc = Math.min(loc, dst);
                // System.out.printf("%d -> %d\n", sid, loc);
            }
        }
        return loc;
    }
}
