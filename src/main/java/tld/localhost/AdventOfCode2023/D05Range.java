package tld.localhost.AdventOfCode2023;

import java.util.OptionalLong;

record D05Range(long dst, long src, long len) {

    public static D05Range fromString(final String src) {
        final String[] nums = src.split(" +");
        final long p = Long.parseLong(nums[0]);
        final long q = Long.parseLong(nums[1]);
        final long r = Long.parseLong(nums[2]);
        return new D05Range(p, q, r);
    }

    public OptionalLong apply(final long val) {
        if (src > val) return OptionalLong.empty();
        final long off = val - src;
        if (len < off) return OptionalLong.empty();
        return OptionalLong.of(dst + off);
    }

    public String toString() {
        return String.format("{%d %d %d}", src, dst, len);
    }
}
