package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;

/*
 * AoC 2023
 * Day 22
 * Sand Slabs
 * TODO: 1 2
 */
public class Day22 {
    final static String FILE = "test/22-1.txt";
    // final static String FILE = "22.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day22.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // todo
        System.out.println(part2(lines)); // todo
    }

    /**
     * Figure how the blocks will settle based on the snapshot.
     * Once they've settled, consider disintegrating a single brick;
     * how many bricks could be safely chosen as the one to get disintegrated?
     */
    private static long part1(final List<String> lines) { // todo
        return -1;
    }

    /**
     *
     */
    private static long part2(final List<String> lines) { // todo
        return -1;
    }
}
