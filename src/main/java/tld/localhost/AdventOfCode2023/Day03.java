package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Character.isDigit;

/*
 * AoC 2023
 * Day 3
 * Gear Ratios
 * What is the sum of all of the part numbers in the engine schematic?
 * TODO: 2
 */
public class Day03 {
    // final static String FILE = "test/03-1.txt";
    final static String FILE = "03.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day03.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines)); // todo
    }

    /**
     * What is the sum of all of the part numbers in the engine schematic?
     */
    private static long part1(final List<String> lines) { // todo
        final byte[][] engineMap = makeEngineMap(lines);
        long sum = 0;
        for(int row = 0; row < engineMap.length; ++row) {
            int partNumber = 0;
            int symbolTag = 0;
            for(int col = 0; col < engineMap[row].length; ++col) {
                final byte b = engineMap[row][col];
                if(isDigit(b)) {
                    partNumber = partNumber * 10 + (b - '0');
                    if(checkNeighbourhs(engineMap, row, col) > 0)
                        ++symbolTag;
                }
                else {
                    if(partNumber > 0) {
                        if(symbolTag > 0)
                            sum += partNumber;
                        // System.out.printf("%d %d %c\n", partNumber, symbolTag, b);
                        symbolTag = 0;
                        partNumber = 0;
                    }
                }
            }
            if(partNumber > 0) {
                if (symbolTag > 0)
                    sum += partNumber;
                // System.out.printf("%d %d -- tailcatch\n", partNumber, symbolTag);
            }
        }
        return sum;
    }

    final static int[] OFFS = {-1,-1, 1,1, -1,1, 1,-1, -1,0, 1,0, 0,-1, 0,1,};
    /**
     * check surroundings for any symbol presence
     * @return true if any symbol adjacent this cell
     */
    private static int checkNeighbourhs(final byte[][] engineMap, final int row, final int col) { // todo
        int symF = 0;
        for(int i = 0; i < OFFS.length; i += 2) {
            final int y = row + OFFS[i];
            if(y < 0 || y >= engineMap.length) continue;
            final int x = col + OFFS[i+1];
            if(x < 0 || x >= engineMap[y].length) continue;
            if(isSymbol(engineMap[y][x]))
                ++symF;
        }
        return symF;
    }

    private static byte[][] makeEngineMap(final List<String> lines) {
        byte[][] engineMap = new byte[lines.size()][];
        for(int i = 0; i < lines.size(); ++i)
            engineMap[i] = lines.get(i).getBytes();
        return engineMap;
    }

    private static boolean isSymbol(final byte c) {
        return c != '.' && (c < '0' || c > '9');
    }

    /**
     * A gear is any * symbol that is adjacent to exactly two part numbers.
     * Its gear ratio is the result of multiplying those two numbers together.
     */
    private static int part2(final List<String> lines) { // todo
        return -1;
    }
}
