package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * AoC 2023
 * Day 1
 * Trebuchet?!
 * What is the sum of all of the calibration values?
 * TODO: 2
 */
public class Day01 {
    // final static String FILE = "test01-1.txt";
    final static String FILE = "01.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day01.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines)); // ok
        System.out.println(part2(lines)); // todo
    }

    final static Pattern headP = Pattern.compile("^[a-z]*?([0-9])");
    final static Pattern tailP = Pattern.compile("([0-9])[a-z]*?$");
    private static int part1(final List<String> lines) {
        int sum = 0;
        for(String str: lines) {
            Matcher m = headP.matcher(str);
            if(m.find() == false) continue;
            final int h = Integer.parseInt(m.group(1));
            m = tailP.matcher(str);
            if(m.find() == false) continue;
            final int t = Integer.parseInt(m.group(1));
            sum += h * 10 + t;
        }
        return sum;
    }

    static final String[] numerals = {"seven", "eight", "three", "four", "five", "nine", "six", "one", "two",};
    final static int[] numbers = {7, 8, 3, 4, 5, 9, 6, 1, 2};
    private static int part2(final List<String> lines) { // todo
        Map<String, D01NumVal> trie = new HashMap<>(10);
        for(int i = 0; i < numerals.length; ++i)
            trie.put(numerals[i].substring(0,2), new D01NumVal(numerals[i], numbers[i]));
        int sum = 0;
        for(String sline : lines) {
            final String str = numeralizeString(sline, trie);
            Matcher m = headP.matcher(str);
            if(m.find() == false) continue;
            final int h = Integer.parseInt(m.group(1));
            m = tailP.matcher(str);
            if(m.find() == false) continue;
            final int t = Integer.parseInt(m.group(1));
            sum += h * 10 + t;
            System.out.printf("%s -> %s : %1d%1d\n", sline, str, h, t);
        }
        return sum;
    }

    private static String numeralizeString(final String str, final Map<String, D01NumVal> trie) {
        StringBuilder sb = new StringBuilder(str.length());
        for(int i = 0; i < str.length(); ++i) {
            if(i+2 >= str.length()) {
                sb.append(str.charAt(i));
                continue;
            }
            final String pfx = str.substring(i, i + 2);
            // System.out.println(pfx);
            if(trie.containsKey(pfx)) {
                final D01NumVal kv = trie.get(pfx);
                final String key = kv.key();
                final int val = kv.val();
                if((i + key.length()) <= str.length() && str.substring(i, i + key.length()).equals(key)) {
                    sb.append(val);
                    i += key.length() - 1;
                    continue;
                }
            }
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }
}
