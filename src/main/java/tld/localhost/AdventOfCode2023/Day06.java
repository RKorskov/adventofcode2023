package tld.localhost.AdventOfCode2023;

import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
 * AoC 2023
 * Day 6
 * Wait For It
 * TODO: 2
 */
public class Day06 {
    // final static String FILE = "test/06-1.txt";
    final static String FILE = "06.txt";

    public static void main(final String[] args) throws IOException {
        Scanner sc = new Scanner(Day06.class.getClassLoader().getResourceAsStream(FILE));
        List<String> lines = new ArrayList<>();
        for(;sc.hasNextLine();)
            lines.add(sc.nextLine());
        System.out.println(part1(lines));
        System.out.println(part2(lines));
    }

    /**
     * Your toy boat has a starting speed of zero millimeters per millisecond.
     * For each whole millisecond you spend at the beginning of the race holding down the button,
     * the boat's speed increases by one millimeter per millisecond.
     * Determine the number of ways you could beat the record in each race.
     * What do you get if you multiply these numbers together?
     */
    private static long part1(final List<String> lines) {
        final int[] time = Stream.of(lines.get(0).split(":? +")).skip(1).mapToInt(Integer::parseUnsignedInt).toArray();
        final int[] dist = Stream.of(lines.get(1).split(":? +")).skip(1).mapToInt(Integer::parseUnsignedInt).toArray();
        final int[] tRec = new int[time.length];
        long acc = 1;
        for(int raceIdx = 0; raceIdx < time.length; ++raceIdx) {
            for(int buttonPressTime = 1; buttonPressTime < time[raceIdx]; ++buttonPressTime) {
                final int speed = buttonPressTime;
                final int d = speed * (time[raceIdx] - buttonPressTime);
                if(d > dist[raceIdx])
                    ++tRec[raceIdx];
            }
        }
        for(int i = 0; i < tRec.length; ++i)
            if(tRec[i] > 0)
                acc *= tRec[i];
        return acc;
    }

    /**
     * There's really only one race - ignore the spaces between the numbers on each line.
     * How many ways can you beat the record in this one much longer race?
     */
    private static long part2(final List<String> lines) {
        long[] dt = new long[lines.size()];
        for(int j = 0; j < lines.size(); ++j) {
            final String line = lines.get(j);
            final String[] times = line.split(" +");
            StringBuilder sb = new StringBuilder(line.length());
            for(int i = 1; i < times.length; ++i)
                sb.append(times[i]);
            dt[j] = Long.parseLong(sb.toString());
        }
        final long raceTime = dt[0];
        final long raceDist = dt[1];
        System.out.printf("time: %d   dist: %d\n", raceTime, raceDist);
        long acc = 0;
        for(long buttonPressTime = 1; buttonPressTime < raceTime; ++buttonPressTime) {
            final long speed = buttonPressTime;
            final long d = speed * (raceTime - buttonPressTime);
            if(d > raceDist)
                ++acc;
        }
        return acc;
    }
}
